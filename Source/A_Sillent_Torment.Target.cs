// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class A_Sillent_TormentTarget : TargetRules
{
	public A_Sillent_TormentTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "A_Sillent_Torment" } );
	}
}
